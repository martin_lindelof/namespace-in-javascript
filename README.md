## very small namespace example in javascript

This super small examples consist of 3 files, a clear entry point named main.js

Regarding main.js I think that´s how I will start ALL of my javascript project and
try to resemble C++ coding as much as possible.

The other two files are an ENGINE and a GUI, in different namespaces.
They can communicate inbetween using vars and methods exposed in a "INTERFACE"
always declared at top.

## making a private function, public.

So you have a function like so

----------------------------

function setup (argument) {
	// do something
}

----------------------------

and you want to expose it, in the "INTERFACE" part of the JS file (in top).
Now I try to have it as close to a .h HEADER file in C++

----------------------------

// INTERFACE
// functions
this.setup = setup;


function setup (argument) {
	// do something
}

----------------------------

that way you can typ functions as you usually just do, and in the "HEADER"
or "INTERFACE" part at the top you can quite easy turn on/off vars and methods
exposed for this namespace.

