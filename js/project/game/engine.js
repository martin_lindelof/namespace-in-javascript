project = window.project || {};
project.game = window.project.game || {};

project.game.engine = new function(){

	// -----------------------------
	// HEADER
	// public functions
	this.start = start;

	// getter setters
	this.getMach = getMach;
	this.setmach = setMach;
	this.getSpeed = getSpeed;
	this.setSpeed = setSpeed;

	// -----------------------------

	var GUI;

	// private vars
	var width, height;
	var speed, x, y, z;
	var mach = "mach is over 9000";

	// public function
	function start (argument) {
		// body...
		console.log("Starting game engine");
		document.body.innerHTML += '<p>Starting game engine</p>';

		GUI = project.game.gui;

		turnKey();
		spawnShip();
	}

	// private function
	function turnKey (argument) {
		// body...
		console.log("Turns key");
		document.body.innerHTML += '<p>Turns key</p>';
		speed = -1;
	}

	// private function
	function spawnShip (argument) {
		// body...
		console.log("Spawns ship");
		document.body.innerHTML += '<p>Spawns ship</p>';
		GUI.showMsg();
	}

	// getter setters
	function setSpeed (argument) {
		speed = argument;
		console.log("Speed set to: " +argument);
		document.body.innerHTML += '<p>Speed set to: '+argument+'</p>';
	}

	function getSpeed () {
		return speed;
	}

	function getMach () {
		return mach;
	}

	function setMach (argument) {
		mach = argument;
	}



}