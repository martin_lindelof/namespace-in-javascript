project = window.project || {};
project.main = new function(){

	// -----------------------------
	// HEADER
	this.init = init; // entry point for the DOM

	// -----------------------------

	var GUI;
	var ENGINE;

	var something = 9;
	var more = 5;

	// private function
	function init (argument) {
		// body...

		console.log("Initialize ...");
		document.body.innerHTML += '<p>Initialize ...</p>'

		ENGINE = project.game.engine;
		GUI = project.game.gui;


		ENGINE.start();
		GUI.start();
	}

	function update (argument) {
		// body...
		something = 10;
	}

	function draw (argument) {
		// body...
		more = 1337;
	}

};
